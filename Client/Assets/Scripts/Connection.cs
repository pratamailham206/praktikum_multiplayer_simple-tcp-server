using UnityEngine.UI;
using UnityEngine;
using System.Net.Sockets;
using System.IO;
using System.Threading;

public class Connection : MonoBehaviour
{
    private TcpClient client;
    private StreamReader reader;
    private StreamWriter writer;

    public GameObject onConnect;
    public GameObject ConnectBtn;
    public InputField input;
    public Text messageText;

    private string ClientMessage;

    public void ConnectToServer()
    {
        client = new TcpClient("127.0.0.1", 8080);
        Debug.Log(" [Berhasil Terhubung...]");

        reader = new StreamReader(client.GetStream());
        writer = new StreamWriter(client.GetStream());

        Thread Client = new Thread(RecvData);
        Client.Start();

        ConnectBtn.SetActive(false);
        onConnect.SetActive(true);
    }

    public void SendData()
    {
        writer.WriteLine(input.text.ToString());
        writer.Flush();
    }

    private void Update()
    {
        messageText.text = ClientMessage;
    }

    private void RecvData()
    {
        while(true)
        {
            ClientMessage = reader.ReadLine();
        }
    }
}
